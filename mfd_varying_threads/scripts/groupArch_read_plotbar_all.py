#!/usr/bin/env python
import numpy as np
from matplotlib.pyplot import figure, savefig, plot, show, legend, xlabel, ylabel
from os.path import expanduser

import matplotlib.patches as mpatches

HOME = expanduser("~")
#in_lable_knl="knl_puck"
#in_lable_has="haswell_puck"
#in_lable_knl="knl_small"
#in_lable_has="haswell_small"
PNG_PATH=HOME+"/results_truchas/data/data/poster_varying_threads/png/"
input_path=HOME+"/results_truchas/data/data/poster_varying_threads/all_data/"
PNG_NAME="all_cpu_gpu.png"
#png_name="C_both_puck_has64_loglog.png"
fig = figure(figsize=(10,5))
ax = fig.add_subplot(111)

def read_file_cpu(input_file,kernel_iters):
    hannel_values = open(input_path+input_file).read().split()
    iters =kernel_iters
    size=len(hannel_values)//iters//5
    print("size ", size, "len(hannel_values) ", len(hannel_values))
    nub_threads_list =[]
    avg_iter_list=[]
    avg_time_list=[]
    time_iter_list=[]
    for j in range(iters):
        num_iter=[]
        time=[]
        for i in range(size):
            #print("i ",i)
            num_iter.append(int(hannel_values[1+i*5+j*50]))
            time.append(float(hannel_values[4+i*5+j*50]))
        avg_iter = np.mean(num_iter)
        avg_time = np.mean(time)
    return avg_time/avg_iter


def read_file_gpu(input_file,kernel_iters):
    hannel_values = open(input_path+input_file).read().split()
    iters =kernel_iters
    size=len(hannel_values)//iters//4
    print("size ", size, "len(hannel_values) ", len(hannel_values))
    nub_threads_list =[]
    avg_iter_list=[]
    avg_time_list=[]
    time_iter_list=[]
    for j in range(iters):
        num_iter=[]
        time=[]
        for i in range(size):
            #print("i ",i)
            num_iter.append(int(hannel_values[0+i*4+j*40]))
            time.append(float(hannel_values[3+i*4+j*40]))
        avg_iter = np.mean(num_iter)
        avg_time = np.mean(time)
    return avg_time/avg_iter





###################################################################################
#PLOT
def plot_threads(knl_nub_threads_list,knl_time_iter_list,has_nub_threads_list,has_time_iter_list):
    ax.plot(has_nub_threads_list, has_time_iter_list/has_time_iter_list[0], label=in_lable_has)
    ax.plot(knl_nub_threads_list, knl_time_iter_list/knl_time_iter_list[0], label=in_lable_knl)
    x_min=int(knl_nub_threads_list[0])
    x_max=int(knl_nub_threads_list[-1])
    x=[x_min,x_max]
    y=[x_min**-1,x_max**-1]
    ax.plot(x, y, label='ideal scaling line',linestyle='--')
    ######### Plotting adjustments ##########
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlabel("# of threads",fontsize=30)
    ax.set_ylabel("Relative Time",fontsize=30,labelpad=15)
    ax.legend(frameon=False,handlelength=4,fontsize=20,loc=3)
    ax.xaxis.set_tick_params(labelsize=20)
    ax.yaxis.set_tick_params(labelsize=20)
    fig.tight_layout()
    savefig(png_path+png_name,dpi=400)


def plot_bar(labels, data):
    fig = figure(figsize=(10, 5))
    ax = fig.add_subplot(111)

    # Evenly space plots
    #index = np.arange(len(labels))
    #index=[0,0.6,1.2,1.8, 2.8,3.4, 4.4, 5.4 ]
    index=[-0.2,0.406,1.012,1.618, 2.6,3.206, 4.2, 5.2 ]
    print("index ",index)
    #index[1,5] - 0.1
    #index = [x-0.1 for (i,x) in enumerate(index) if i<5]
    #opacity = 0.4
    #error_config = {'ecolor': '0.3'}

    barlist = ax.bar(index, data, align='center', width=0.6)
    #barlist = ax.bar(index, data, align='center', alpha=opacity,error_kw=error_config)
    #red:
    barlist[3].set_color('#d62728')
    barlist[7].set_color('#d62728')
    barlist[6].set_color('#d62728')
    barlist[4].set_color('#d62728')
    barlist[0].set_color('#d62728')
    '''
    #blue:
    barlist[3].set_color('#31727B')
    barlist[7].set_color('#31727B')
    barlist[6].set_color('#31727B')
    barlist[4].set_color('#31727B')
    barlist[0].set_color('#31727B')
    '''
    #Nivida green(cuda)
    barlist[5].set_color('#84B737')
    barlist[2].set_color('#84B737')
    #organge for offload
    barlist[1].set_color('#ff7f0e')
    #openmp = mpatches.Patch(color='#31727B', label='OpenMP')
    openmp = mpatches.Patch(color='#d62728', label='OpenMP')
    cuda = mpatches.Patch(color='#84B737', label='Cuda')
    offload = mpatches.Patch(color='#ff7f0e', label='OpenMP Offload')
    ######### Plot configuration ##########
    ax.set_xticks(index)
    ax.set_xticklabels(labels,fontsize=15,rotation = 45, ha="right", position=(0,-0.03))
    #ax.set_xticklabels(labels,fontsize=10,rotation = 45, va="bottom", position=(0,-0.3))
    ax.set_title("Performance of Optimization Approaches",fontsize=30)
    #ax.set_xlabel("Parallel Approaches")
    ax.set_ylabel("Run Time (Seconds)",fontsize=20)
    #ax.set_title('Performance of Cell-centered Gradient Kernel on ' + VARIANT_TITLE + ' with different vector instructions')
    legend(handles=[openmp,cuda,offload], loc='upper right')
    fig.tight_layout()
    savefig(PNG_PATH+'/'+PNG_NAME,dpi=400)
    show()



exitapp = False
if __name__ == '__main__':
    try:  
        #Passing the knl data file name here, 19 is the  # of different threads number for knl up to 272
        data =[]
        #labels=['Broadwell_OpenMP','Haswell_OpenMP','knl_OpenMP', 'Power9GCC_OpenMP','Power9IBM_OpenMP','Cuda_power9','Cuda_volta-x86','IBM_offload']
        #labels=['Power9GCC_OpenMP','IBM_offload','Cuda_power9','Power9IBM_OpenMP','Broadwell_OpenMP','Cuda_volta-x86','knl_OpenMP','Haswell_OpenMP']
        labels=['Power9 GCC','Power9 IBM','Power9 NVCC','Power9 IBM','volta-x86 Intel','volta-x86 INVCC','KNL Intel','Haswell Intel']
        #Read data in lable order for grouping by node model
        data.append(read_file_cpu("bar160_power9_gcc.data",1))
        data.append(read_file_gpu("offload_ibm_puck_varying_threads_2018-07-18_10-32-44.data",1))
        data.append(read_file_gpu("power9_cuda_puck_varying_threads_2018-07-10_17-03-06.data",1))
        data.append(read_file_cpu("bar160_power9ibm.data",1))
        data.append(read_file_cpu("bar72_broadwell.data",1))
        data.append(read_file_gpu("cuda_votas_puck_varying_threads_2018-07-18_13-08-01.data",1))
        data.append(read_file_cpu("bar272_knl_intel_cores_openmp.data",1))
        data.append(read_file_cpu("bar144_haswell_intel_cores_openmp.data",1))
        plot_bar(labels, data)
        print("done")
    except KeyboardInterrupt:
        exitapp = True
        raise             
