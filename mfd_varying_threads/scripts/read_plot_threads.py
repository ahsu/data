#!/usr/bin/env python
import numpy as np
from matplotlib.pyplot import figure, savefig, plot, show, legend, xlabel, ylabel
from os.path import expanduser


in_lable_knl="KNL(272)"
in_lable_has="Haswell 4S(144)"
in_lable_hascori="Haswell 2S(64)"
in_lable_broad="Broadwell(72)"
in_lable_power9gcc="Power9 gfortran(160)"
in_lable_power9ibm="Power9 xlf(160)"

HOME = expanduser("~")
PNG_PATH=HOME+"/results_truchas/data/data/poster_varying_threads/png/"
input_path=HOME+"/results_truchas/data/data/poster_varying_threads/"
#PNG_NAME="all5_cpu_openmp_scaling.png"
PNG_NAME="all_cpu_openmp_realtime.png"
fig = figure(figsize=(10,5))
ax = fig.add_subplot(111)
#ax2 = ax.twinx()

def read_file(input_file,kernel_iters):
	hannel_values = open(input_path+input_file).read().split()
	iters =kernel_iters
	size=len(hannel_values)//iters//5
	print("size ", size, "len(hannel_values) ", len(hannel_values))
	nub_threads_list =[]
	avg_iter_list=[]
	avg_time_list=[]
	time_iter_list=[]
	for j in range(iters):
		num_iter=[]
		time=[]
		for i in range(size):
			#print("i ",i)
			num_iter.append(int(hannel_values[1+i*5+j*50]))
			time.append(float(hannel_values[4+i*5+j*50]))
		avg_iter = np.mean(num_iter)
		avg_time = np.mean(time)
		nub_threads_list.append(hannel_values[j*50])
		avg_iter_list.append(avg_iter)
		avg_time_list.append(avg_time)
		time_iter_list.append(avg_time/avg_iter)
		print(hannel_values[j*50],avg_iter, avg_time, avg_time/avg_iter)
		#print(hannel_values[j*50],avg_iter, avg_time, avg_time/avg_iter, file=out_file)
	return nub_threads_list,avg_iter_list,avg_time_list,time_iter_list




###################################################################################
#PLOT
def plot_threads(knl_nub_threads_list,knl_time_iter_list,has_nub_threads_list,has_time_iter_list):
	ax.plot(has_nub_threads_list, has_time_iter_list/has_time_iter_list[0], label=in_lable_has)
	ax.plot(knl_nub_threads_list, knl_time_iter_list/knl_time_iter_list[0], label=in_lable_knl)
	x_min=int(knl_nub_threads_list[0])
	x_max=int(knl_nub_threads_list[-1])
	x=[x_min,x_max]
	y=[x_min**-1,x_max**-1]
	ax.plot(x, y, label='ideal scaling line',linestyle='--')
	######### Plotting adjustments ##########
	ax.set_xscale('log')
	ax.set_yscale('log')
	ax.set_xlabel("# of threads",fontsize=30)
	ax.set_ylabel("Relative Time",fontsize=30,labelpad=15)
	ax.legend(frameon=False,handlelength=4,fontsize=20,loc=3)
	ax.xaxis.set_tick_params(labelsize=20)
	ax.yaxis.set_tick_params(labelsize=20)
	fig.tight_layout()
	savefig(PNG_PATH+'/'+PNG_NAME,dpi=400)
	show()



def plot_all4_threads(knl_nub_threads_list,knl_time_iter_list,has_nub_threads_list,has_time_iter_list,broad_nub_threads_list,broad_time_iter_list,power9_nub_threads_list,power9_time_iter_list):
	ax.plot(has_nub_threads_list, has_time_iter_list/has_time_iter_list[0], label=in_lable_has)
	ax.plot(knl_nub_threads_list, knl_time_iter_list/knl_time_iter_list[0], label=in_lable_knl)
	ax.plot(broad_nub_threads_list, broad_time_iter_list/broad_time_iter_list[0], label=in_lable_broad)
	ax.plot(power9_nub_threads_list, power9_time_iter_list/power9_time_iter_list[0], label=in_lable_power9)
	x_min=int(knl_nub_threads_list[0])
	x_max=int(knl_nub_threads_list[-1])
	x=[x_min,x_max]
	y=[x_min**-1,x_max**-1]
	ax.plot(x, y, label='ideal scaling line',linestyle='--')
	######### Plotting adjustments ##########
	ax.set_xscale('log')
	ax.set_yscale('log')
	ax.set_xlabel("# of threads",fontsize=30)
	ax.set_ylabel("Relative Time",fontsize=30,labelpad=15)
	ax.legend(frameon=False,handlelength=4,fontsize=20,loc=3)
	ax.xaxis.set_tick_params(labelsize=20)
	ax.yaxis.set_tick_params(labelsize=20)
	fig.tight_layout()
	savefig(PNG_PATH+'/'+PNG_NAME,dpi=400)
	show()

#scaling plot only
def plot_all5_threads(knl_nub_threads_list,knl_time_iter_list,has_nub_threads_list,has_time_iter_list,broad_nub_threads_list,broad_time_iter_list,power9gcc_nub_threads_list,power9gcc_time_iter_list,power9ibm_nub_threads_list,power9ibm_time_iter_list):
	ax.plot(has_nub_threads_list, has_time_iter_list/has_time_iter_list[0], label=in_lable_has,linewidth=2)
	ax.plot(knl_nub_threads_list, knl_time_iter_list/knl_time_iter_list[0], label=in_lable_knl,linewidth=2)
	ax.plot(broad_nub_threads_list, broad_time_iter_list/broad_time_iter_list[0], label=in_lable_broad,linewidth=2)
	ax.plot(power9gcc_nub_threads_list, power9gcc_time_iter_list/power9gcc_time_iter_list[0], label=in_lable_power9gcc,linewidth=2)
	ax.plot(power9ibm_nub_threads_list, power9ibm_time_iter_list/power9ibm_time_iter_list[0], label=in_lable_power9ibm,linewidth=2)
	x_min=int(knl_nub_threads_list[0])
	x_max=int(knl_nub_threads_list[-1])
	x=[x_min,x_max]
	y=[x_min**-1,x_max**-1]
	ax.plot(x, y, label='Ideal Scaling Line',linestyle='--')
	######### Plotting adjustments ##########
	ax.set_xscale('log')
	ax.set_yscale('log')
	ax.set_xlabel("Number of Threads",fontsize=20)
	ax.set_ylabel("Relative Time",fontsize=20,labelpad=15)
	ax.set_title("Performance Scaling with OpenMP",fontsize=30)
	ax.legend(frameon=False,handlelength=2,fontsize=15,loc=3)
	ax.xaxis.set_tick_params(labelsize=20)
	ax.yaxis.set_tick_params(labelsize=20)
	fig.tight_layout()
	savefig(PNG_PATH+'/'+PNG_NAME,dpi=400)
	show()

#scaling and real time in one plot with 2 y-axis
def plot_all5_scale_realtime_threads(knl_nub_threads_list,knl_time_iter_list,has_nub_threads_list,has_time_iter_list,broad_nub_threads_list,broad_time_iter_list,power9gcc_nub_threads_list,power9gcc_time_iter_list,power9ibm_nub_threads_list,power9ibm_time_iter_list):
	ax.plot(has_nub_threads_list, has_time_iter_list/has_time_iter_list[0], label=in_lable_has)
	ax.plot(knl_nub_threads_list, knl_time_iter_list/knl_time_iter_list[0], label=in_lable_knl)
	ax.plot(broad_nub_threads_list, broad_time_iter_list/broad_time_iter_list[0], label=in_lable_broad)
	ax.plot(power9gcc_nub_threads_list, power9gcc_time_iter_list/power9gcc_time_iter_list[0], label=in_lable_power9gcc)
	ax.plot(power9ibm_nub_threads_list, power9ibm_time_iter_list/power9ibm_time_iter_list[0], label=in_lable_power9ibm)
	#second y axis:
	ax2.plot(has_nub_threads_list, has_time_iter_list, label=in_lable_has)
	ax2.plot(knl_nub_threads_list, knl_time_iter_list, label=in_lable_knl)
	ax2.plot(broad_nub_threads_list, broad_time_iter_list, label=in_lable_broad)
	ax2.plot(power9gcc_nub_threads_list, power9gcc_time_iter_list, label=in_lable_power9gcc)
	ax2.plot(power9ibm_nub_threads_list, power9ibm_time_iter_list, label=in_lable_power9ibm)
	x_min=int(knl_nub_threads_list[0])
	x_max=int(knl_nub_threads_list[-1])
	x=[x_min,x_max]
	y=[x_min**-1,x_max**-1]
	ax.plot(x, y, label='ideal scaling line',linestyle='--')
	######### Plotting adjustments ##########
	ax.set_xscale('log')
	ax.set_yscale('log')
	ax.set_xlabel("Number of threads",fontsize=20)
	ax.set_ylabel("Relative Time",fontsize=20,labelpad=15)
	ax.legend(frameon=False,handlelength=4,fontsize=18,loc=3)
	ax.xaxis.set_tick_params(labelsize=20)
	ax.yaxis.set_tick_params(labelsize=20)
	ax2.set_xscale('log')
	ax2.set_yscale('log')
	ax2.set_ylabel("seconds",fontsize=20,labelpad=15)
	ax.yaxis.set_tick_params(labelsize=20)
	ax2.yaxis.set_tick_params(labelsize=20)
	fig.tight_layout()
	savefig(PNG_PATH+'/'+PNG_NAME,dpi=400)
	show()





def plot_realtime_all4_threads(knl_nub_threads_list,knl_time_iter_list,has_nub_threads_list,has_time_iter_list,broad_nub_threads_list,broad_time_iter_list,power9_nub_threads_list,power9_time_iter_list):
	ax.plot(has_nub_threads_list, has_time_iter_list, label=in_lable_has)
	ax.plot(knl_nub_threads_list, knl_time_iter_list, label=in_lable_knl)
	ax.plot(broad_nub_threads_list, broad_time_iter_list, label=in_lable_broad)
	ax.plot(power9_nub_threads_list, power9_time_iter_list, label=in_lable_power9)
	x_min=int(knl_nub_threads_list[0])
	x_max=int(knl_nub_threads_list[-1])
	x=[x_min,x_max]
	y=[x_min**-1,x_max**-1]
	ax.plot(x, y, label='ideal scaling line',linestyle='--')
	######### Plotting adjustments ##########
	ax.set_xscale('log')
	ax.set_yscale('log')
	ax.set_xlabel("# of threads",fontsize=20)
	ax.set_ylabel("Seconds",fontsize=20,labelpad=15)
	ax.legend(frameon=False,handlelength=4,fontsize=20,loc=3)
	ax.xaxis.set_tick_params(labelsize=20)
	ax.yaxis.set_tick_params(labelsize=20)
	fig.tight_layout()
	savefig(PNG_PATH+'/'+PNG_NAME,dpi=400)
	show()

def plot_realtime_all6_threads(knl_nub_threads_list,knl_time_iter_list,has_nub_threads_list,has_time_iter_list,cori_has_nub_threads_list,cori_has_time_iter_list,broad_nub_threads_list,broad_time_iter_list,power9gcc_nub_threads_list,power9gcc_time_iter_list,power9ibm_nub_threads_list,power9ibm_time_iter_list):
	p=ax.plot(cori_has_nub_threads_list, cori_has_time_iter_list, label=in_lable_hascori,linewidth=2)
	ax.plot(has_nub_threads_list, has_time_iter_list, label=in_lable_has,linewidth=2)
	ax.plot(knl_nub_threads_list, knl_time_iter_list, label=in_lable_knl,linewidth=2)
	ax.plot(broad_nub_threads_list, broad_time_iter_list, label=in_lable_broad,linewidth=2)
	ax.plot(power9gcc_nub_threads_list, power9gcc_time_iter_list, label=in_lable_power9gcc,linewidth=2)
	ax.plot(power9ibm_nub_threads_list, power9ibm_time_iter_list, label=in_lable_power9ibm,linewidth=2)
	#haswell cori as base line
	print("cori_has_time_iter_list ", cori_has_time_iter_list[0], "cori_has_nub_threads_list[0] ", cori_has_nub_threads_list[0])
	print("LOG cori_has_time_iter_list ", np.log(0.0501162017435), "cori_has_nub_threads_list[0] ", np.log(1))
	#print("LOG cori_has_time_iter_list ", np.log(cori_has_time_iter_list[0]), "cori_has_nub_threads_list[0] ", np.log(cori_has_nub_threads_list[0]))
	x_min=int(cori_has_nub_threads_list[0])
	x_max=int(cori_has_nub_threads_list[-1])
	x=[x_min,x_max]
	a=cori_has_time_iter_list[0]
	y=[a*(x_min**(-1)),a*(x_max**(-1))]
	#y=[x_min**(-cori_has_time_iter_list[0]),x_max**(-cori_has_time_iter_list[0])]
	#y=[x_min**-1,x_max**-1]
	ax.plot(x, y, linestyle='--',label='Ideal Scaling', color=p[0].get_color())
	######### Plotting adjustments ##########
	ax.set_ylim(0.001,1)
	ax.set_xscale('log')
	ax.set_yscale('log')
	ax.set_xlabel("Number of Threads",fontsize=20)
	ax.set_ylabel("Run Time (Seconds)",fontsize=20,labelpad=15)
	ax.set_title("Performance Scaling with OpenMP CPU",fontsize=30)
	ax.legend(frameon=False,handlelength=4,fontsize=14,loc='upper right')
	ax.xaxis.set_tick_params(labelsize=20)
	ax.yaxis.set_tick_params(labelsize=20)
	fig.tight_layout()
	savefig(PNG_PATH+'/'+PNG_NAME,dpi=400)
	show()





exitapp = False
if __name__ == '__main__':
	try:  
		#Passing the knl data file name here, 19 is the  # of different threads number for knl up to 272
		knl_nub_threads_list,knl_avg_iter_list,knl_avg_time_list,knl_time_iter_list= read_file("Cori_knl_Openmp_2018-07-19_08-47-55.data",19)#cori
		#knl_nub_threads_list,knl_avg_iter_list,knl_avg_time_list,knl_time_iter_list= read_file("knl_intel_cores_openmp_puck_varying_threads_2018-07-17_18-07-39.data",19)#darwin
		assert(len(knl_avg_iter_list)==len(knl_avg_time_list)==len(knl_time_iter_list) == len(knl_nub_threads_list))
		#Passing the haswell data file name here, 9 is the  # of different threads number for haswell up to 64
		has_nub_threads_list,has_avg_iter_list,has_avg_time_list,has_time_iter_list= read_file("haswell_intel_cores_openmp_puck_varying_threads_2018-07-17_17-48-15.data",10)
		assert(len(has_avg_iter_list)==len(has_avg_time_list)==len(has_time_iter_list) == len(has_nub_threads_list))
		cori_has_nub_threads_list,cori_has_avg_iter_list,cori_has_avg_time_list,cori_has_time_iter_list= read_file("Cori_haswell_Openmp_2018-07-19_12-18-33.data",9)
		power9gcc_nub_threads_list,power9gcc_avg_iter_list,power9gcc_avg_time_list,power9gcc_time_iter_list= read_file("power9_gcc_openmp_2018-07-17_16-39-16.data",15)
		assert(len(power9gcc_avg_iter_list)==len(power9gcc_avg_time_list)==len(power9gcc_time_iter_list) == len(power9gcc_nub_threads_list))
		power9ibm_nub_threads_list,power9ibm_avg_iter_list,power9ibm_avg_time_list,power9ibm_time_iter_list= read_file("openmp_ibm_puck_varying_threads_2018-07-18_13-09-45.data",12)
		assert(len(power9ibm_avg_iter_list)==len(power9ibm_avg_time_list)==len(power9ibm_time_iter_list) == len(power9ibm_nub_threads_list))
		broad_nub_threads_list,broad_avg_iter_list,broad_avg_time_list,broad_time_iter_list= read_file("broadwell_intel__openmp_puck_varying_threads_2018-07-18_10-49-19.data",11)
		#broad_nub_threads_list,broad_avg_iter_list,broad_avg_time_list,broad_time_iter_list= read_file("cn410_64_broadwell_intel__openmp_puck_varying_threads_2018-07-25_08-44-58.data",14)
		#broad_nub_threads_list,broad_avg_iter_list,broad_avg_time_list,broad_time_iter_list= read_file("broadwell_intel__openmp_puck_varying_threads_2018-07-18_10-49-19.data",11)
		assert(len(broad_avg_iter_list)==len(broad_avg_time_list)==len(broad_time_iter_list) == len(broad_nub_threads_list))

		#plot_all5_scale_realtime_threads(knl_nub_threads_list,knl_time_iter_list,has_nub_threads_list,has_time_iter_list,broad_nub_threads_list,broad_time_iter_list,power9gcc_nub_threads_list,power9gcc_time_iter_list,power9ibm_nub_threads_list,power9ibm_time_iter_list)
		#plot_all5_threads(knl_nub_threads_list,knl_time_iter_list,has_nub_threads_list,has_time_iter_list,broad_nub_threads_list,broad_time_iter_list,power9gcc_nub_threads_list,power9gcc_time_iter_list,power9ibm_nub_threads_list,power9ibm_time_iter_list)
		plot_realtime_all6_threads(knl_nub_threads_list,knl_time_iter_list,has_nub_threads_list,has_time_iter_list,cori_has_nub_threads_list,cori_has_time_iter_list,broad_nub_threads_list,broad_time_iter_list,power9gcc_nub_threads_list,power9gcc_time_iter_list,power9ibm_nub_threads_list,power9ibm_time_iter_list)
		print("done")
	except KeyboardInterrupt:
		exitapp = True
		raise			 
