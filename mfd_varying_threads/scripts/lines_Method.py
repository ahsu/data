#!/usr/bin/env python
import numpy as np
from matplotlib.pyplot import figure, savefig, plot, show, legend, xlabel, ylabel,gca
from os.path import expanduser

import matplotlib.patches as mpatches
from numpy.random import randint
'''
markers = ['\\alpha', '\\beta', '\gamma', '\sigma','\infty', \
            '\spadesuit', '\heartsuit', '\diamondsuit', '\clubsuit', \
            '\\bigodot', '\\bigotimes', '\\bigoplus', '\imath', '\\bowtie', \
            '\\bigtriangleup', '\\bigtriangledown', '\oslash' \
           '\ast', '\\times', '\circ', '\\bullet', '\star', '+', \
            '\Theta', '\Xi', '\Phi', \
            '\$', '\#', '\%', '\S']
'''
markers = ['b', 'h4', 'h2', 'k', 'p', 'p','v','p', 'p'] 
#OpenMP
BROADWELL = {'marker':'o', 'color':'#31727B', 'data_path':'bar72_broadwell.data', 'label':'Broadwell ifort'}
HASWELL4 = {'marker':'s', 'color':'#31727B', 'data_path':'bar144_haswell_intel_cores_openmp.data','label':'Haswell 4S ifort'}#4 socktes
HASWELL2 = {'marker':'d', 'color':'#31727B', 'data_path':'cori_bar64_haswell_intel_cores_openmp.data','label':'Haswell 2S ifort'}
KNL      = {'marker':'v', 'color':'#31727B', 'data_path':'cori_bar272_knl_intel_cores_openmp.data','label':'KNL ifort'}

P_IBM_MP= {'marker':'>', 'color':'#31727B', 'data_path':'bar160_power9ibm.data','label':'Power9 xlf'}
P_GCC_MP= {'marker':'<', 'color':'#31727B', 'data_path':'bar160_power9_gcc.data','label':'Power9 gfortran'}
#cuda
P_GCC_Cuda= {'marker':'<', 'color':'#84B737', 'data_path':'cuda_votas_puck_varying_threads_2018-07-18_13-08-01.data','label':'Volta Broadwell nvcc'}
P_INTEL_Cuda= {'marker':'^', 'color':'#84B737', 'data_path':'power9_cuda_puck_varying_threads_2018-07-10_17-03-06.data','label':'Volta Power9 nvcc'}
#offload
P_IBM_OFF= {'marker':'>', 'color':'#ff7f0e', 'data_path':'offload_ibm_puck_varying_threads_2018-07-18_10-32-44.data','label':'Volta Power9 xlf'}

def getMarker(i):
    # Use modulus in order not to have the index exceeding the lenght of the list (markers)
    return "$"+markers[i % len(markers)]+"$"

HOME = expanduser("~")
PNG_PATH=HOME+"/results_truchas/data/data/poster_varying_threads/png/"
input_path=HOME+"/results_truchas/data/data/poster_varying_threads/all_data/"
PNG_NAME="lines_cpu_gpu.png"
fig = figure(figsize=(10,5))
ax = fig.add_subplot(111)

def read_file_cpu(input_file):
    hannel_values = open(input_path+input_file).read().split()
    iters =1
    size=len(hannel_values)//iters//5
    #print("size ", size, "len(hannel_values) ", len(hannel_values))
    nub_threads_list =[]
    avg_iter_list=[]
    avg_time_list=[]
    time_iter_list=[]
    for j in range(iters):
        num_iter=[]
        time=[]
        for i in range(size):
            #print("i ",i)
            num_iter.append(int(hannel_values[1+i*5+j*50]))
            time.append(float(hannel_values[4+i*5+j*50]))
        avg_iter = np.mean(num_iter)
        avg_time = np.mean(time)
    return avg_time/avg_iter

def read_file_gpu(input_file):
    hannel_values = open(input_path+input_file).read().split()
    iters = 1
    size=len(hannel_values)//iters//4
    #print("size ", size, "len(hannel_values) ", len(hannel_values))
    nub_threads_list =[]
    avg_iter_list=[]
    avg_time_list=[]
    time_iter_list=[]
    for j in range(iters):
        num_iter=[]
        time=[]
        for i in range(size):
            #print("i ",i)
            num_iter.append(int(hannel_values[0+i*4+j*40]))
            time.append(float(hannel_values[3+i*4+j*40]))
        avg_iter = np.mean(num_iter)
        avg_time = np.mean(time)
    return avg_time/avg_iter



###################################################################################
#PLOT
def plot_bar(labels, data):
    fig = figure(figsize=(10, 5))
    ax = fig.add_subplot(111)

    # Evenly space plots
    #index = np.arange(len(labels))
    #index=[0,0.8,1.6,2.4,3.2, 5,5.8, 7.6]
    #6,2,1
    index=[-0.5,0.13,0.76,1.39,2.02,2.65, 3.8,4.46, 5.4]
    #index=[-0.6,0.03,0.66,1.29,1.92,2.55, 3.8,4.46, 5.4]
    #index=[-0.2,0.406,1.012,1.618, 2.6,3.206, 4.2, 5.2 ]
    print("index ",index)
    #index[1,5] - 0.1
    #index = [x-0.1 for (i,x) in enumerate(index) if i<5]
    #opacity = 0.4
    #error_config = {'ecolor': '0.3'}

    barlist = ax.bar(index, data, align='center', width=0.6)
    #barlist = ax.bar(index, data, align='center', alpha=opacity,error_kw=error_config)
    '''
    #red:
    barlist[0].set_color('#d62728')
    barlist[1].set_color('#d62728')
    barlist[2].set_color('#d62728')
    barlist[3].set_color('#d62728')
    barlist[4].set_color('#d62728')
    barlist[5].set_color('#d62728')
    '''

    #blue:
    barlist[0].set_color(BROADWELL['color'])
    barlist[1].set_color(HASWELL4['color'])
    barlist[2].set_color(HASWELL2['color'])
    barlist[3].set_color(KNL['color'])
    barlist[4].set_color(P_IBM_MP['color'])
    barlist[5].set_color(P_GCC_MP['color'])
    #Nivida green
    barlist[6].set_color(P_GCC_Cuda['color'])
    barlist[7].set_color(P_INTEL_Cuda['color'])

    barlist[8].set_color(P_IBM_OFF['color'])



def plot_lines_scatter(data):
    fig = figure(figsize=(10, 5))
    ax = fig.add_subplot(111)
    #openmp(Intel, IBM, Gcc) ,cuad(Intel, Gcc) ,offload(IBM) in order
    lines=[81, 110, 81, 337, 337, 136]
    #lines=[81, 110, 81, 337, 318, 136]
    #data(Broadwell, Haswell(4 sockets),Haswell(2 sockets),KNL,    power9_ibm, power9_gcc) (volta-x86_intel, power9_gcc)  (power9_ibm)
    #ax.scatter(lines[0],data[0],s=100, marker=(5, 0),color='#31727B')
    ax.scatter(lines[0],data[1],s=70, marker=HASWELL4['marker'],color=HASWELL4['color'],label=HASWELL4['label'])
    #ax.scatter(lines[0],data[1],s=100, marker=HASWELL4['marker'],color=HASWELL4['color'],label=HASWELL4['label'])
    ax.scatter(lines[0],data[3],s=100, marker=KNL['marker'],color=KNL['color'],label=KNL['label'])
    ax.scatter(lines[0],data[2],s=100, marker=HASWELL2['marker'],color=HASWELL2['color'],label=HASWELL2['label'])
    ax.scatter(lines[0],data[0],s=100, marker=BROADWELL['marker'],color=BROADWELL['color'],label=BROADWELL['label'])
    ax.scatter(lines[2],data[5],s=100, marker=P_GCC_MP['marker'],color=P_GCC_MP['color'],label=P_GCC_MP['label'])
    ax.scatter(lines[1],data[4],s=100, marker=P_IBM_MP['marker'],color=P_IBM_MP['color'],label=P_IBM_MP['label'])
    #offload
    ax.scatter(lines[5],data[8],s=100, marker=P_IBM_OFF['marker'],color=P_IBM_OFF['color'],label=P_IBM_OFF['label'])
    #cuda
    ax.scatter(lines[3],data[6],s=100, marker=P_GCC_Cuda['marker'],color=P_GCC_Cuda['color'],label=P_GCC_Cuda['label'])
    ax.scatter(lines[4],data[7],s=100, marker=P_INTEL_Cuda['marker'],color=P_INTEL_Cuda['color'],label=P_INTEL_Cuda['label'])
    #first_legend = legend(scatterpoints = 1,loc='upper center',fontsize=15)
    #gca().add_artist(first_legend)

    xmin = np.min(lines)
    xmax = np.max(lines)
    #print("xmin ", xmin,"  xmax ",  xmax," lines[0] ",lines[0])
    openmp = mpatches.Patch(color='#31727B', label='OpenMP CPU')
    #openmp = mpatches.Patch(color='#d62728', label='OpenMP')
    cuda = mpatches.Patch(color='#84B737', label='CUDA GPU')
    offload = mpatches.Patch(color='#ff7f0e', label='OpenMP GPU')
    #second_legend = legend(handles=[openmp,cuda,offload], loc='upper right',fontsize=12)
    second_legend = legend(handles=[openmp,offload,cuda], loc='upper right',fontsize=12)





    ######### Plot configuration ##########
    #ax.set_xticks(index)
    #ax.set_xticklabels(labels,fontsize=15,rotation = 45, ha="right", position=(0,-0.03))
    ax.set_xlim((xmin-20, xmax+20)) 
    #ax.set_ylim((0.0, 0.008)) 
    ax.set_title("Performance vs Lines Changed",fontsize=30)
    ax.set_xlabel("Number of Lines Changed",fontsize=20)
    ax.set_ylabel("Speed Up Factor",fontsize=20)
    ax.set_title('Performance of Cell-centered Gradient Kernel on ' + VARIANT_TITLE + ' with different vector instructions')
    legend(handles=[openmp,cuda,offload], loc='upper right',fontsize=12)
    fig.tight_layout()
    savefig(PNG_PATH+'/'+PNG_NAME,dpi=400)
    show()



exitapp = False
if __name__ == '__main__':
    try:  
        #Passing the knl data file name here, 19 is the  # of different threads number for knl up to 272
        data =[]
        #labels=['Broadwell ifort','Haswell 4Sockets ifort','Haswell 2Sockets ifort','KNL ifort', 'Power9 gfortran','Power9 xlf','volta-x86 ifort nvcc','Power9 ifort nvcc','Power9 xlf']
        cori_has =read_file_gpu("Cori_master_haswell_puck_varying_threads_2018-07-20_00-03-14.data")
        #Read CPU data
        data.append(float("{0:.2f}".format(round(cori_has/read_file_cpu(BROADWELL['data_path'])),2)))
        data.append(float("{0:.2f}".format(round(cori_has/read_file_cpu(HASWELL4['data_path'])),2)))#4 cores
        data.append(float("{0:.2f}".format(round(cori_has/read_file_cpu(HASWELL2['data_path'])),2)))#2 cores
        data.append(float("{0:.2f}".format(round(cori_has/read_file_cpu(KNL['data_path'])),2)))        #knl on cori
        data.append(float("{0:.2f}".format(round(cori_has/read_file_cpu(P_IBM_MP['data_path'])),2)))
        data.append(float("{0:.2f}".format(round(cori_has/read_file_cpu(P_GCC_MP['data_path'])),2)))
        #Read GPU data
        data.append(float("{0:.2f}".format(round(cori_has/read_file_gpu(P_GCC_Cuda['data_path'])),2)))
        data.append(float("{0:.2f}".format(round(cori_has/read_file_gpu(P_INTEL_Cuda['data_path'])),2)))
        data.append(float("{0:.2f}".format(round(cori_has/read_file_gpu(P_IBM_OFF['data_path'])),2)))
        #plot_bar(labels, data)
        plot_lines_scatter(data)
        print("done")
    except KeyboardInterrupt:
        exitapp = True
        raise             
