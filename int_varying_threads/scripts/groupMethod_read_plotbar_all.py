#!/usr/bin/env python
import numpy as np
from matplotlib.pyplot import figure, savefig, plot, show, legend, xlabel, ylabel
from os.path import expanduser

import matplotlib.patches as mpatches

HOME = expanduser("~")
#in_lable_knl="knl_puck"
#in_lable_has="haswell_puck"
#in_lable_knl="knl_small"
#in_lable_has="haswell_small"
PNG_PATH=HOME+"/results_truchas/data/data/poster_varying_threads/png/"
input_path=HOME+"/results_truchas/data/data/poster_varying_threads/all_data/"
PNG_NAME="all_performance.png"
#png_name="C_both_puck_has64_loglog.png"
fig = figure(figsize=(10,5))
ax = fig.add_subplot(111)

def read_file_cpu(input_file,kernel_iters):
    hannel_values = open(input_path+input_file).read().split()
    iters =kernel_iters
    size=len(hannel_values)//iters//5
    print("size ", size, "len(hannel_values) ", len(hannel_values))
    nub_threads_list =[]
    avg_iter_list=[]
    avg_time_list=[]
    time_iter_list=[]
    for j in range(iters):
        num_iter=[]
        time=[]
        for i in range(size):
            #print("i ",i)
            num_iter.append(int(hannel_values[1+i*5+j*50]))
            time.append(float(hannel_values[4+i*5+j*50]))
        avg_iter = np.mean(num_iter)
        avg_time = np.mean(time)
    return avg_time/avg_iter

def read_file_gpu(input_file,kernel_iters):
    hannel_values = open(input_path+input_file).read().split()
    iters =kernel_iters
    size=len(hannel_values)//iters//4
    print("size ", size, "len(hannel_values) ", len(hannel_values))
    nub_threads_list =[]
    avg_iter_list=[]
    avg_time_list=[]
    time_iter_list=[]
    for j in range(iters):
        num_iter=[]
        time=[]
        for i in range(size):
            #print("i ",i)
            num_iter.append(int(hannel_values[0+i*4+j*40]))
            time.append(float(hannel_values[3+i*4+j*40]))
        avg_iter = np.mean(num_iter)
        avg_time = np.mean(time)
    return avg_time/avg_iter

###################################################################################
#PLOT
def plot_threads(knl_nub_threads_list,knl_time_iter_list,has_nub_threads_list,has_time_iter_list):
    ax.plot(has_nub_threads_list, has_time_iter_list/has_time_iter_list[0], label=in_lable_has)
    ax.plot(knl_nub_threads_list, knl_time_iter_list/knl_time_iter_list[0], label=in_lable_knl)
    x_min=int(knl_nub_threads_list[0])
    x_max=int(knl_nub_threads_list[-1])
    x=[x_min,x_max]
    y=[x_min**-1,x_max**-1]
    ax.plot(x, y, label='ideal scaling line',linestyle='--')
    ######### Plotting adjustments ##########
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlabel("# of threads",fontsize=30)
    ax.set_ylabel("Relative Time",fontsize=30,labelpad=15)
    ax.legend(frameon=False,handlelength=4,fontsize=20,loc=3)
    ax.xaxis.set_tick_params(labelsize=20)
    ax.yaxis.set_tick_params(labelsize=20)
    fig.tight_layout()
    savefig(png_path+png_name,dpi=400)


def plot_bar(labels, data):
    fig = figure(figsize=(10, 5))
    ax = fig.add_subplot(111)

    # Evenly space plots
    #index = np.arange(len(labels))
    #index=[0,0.8,1.6,2.4,3.2, 5,5.8, 7.6]
    #6,2,1
    #index=[-0.5,0.13,0.76,1.39,2.02,2.65, 3.8,4.46, 5.4]
    index=[-0.5,0.13,0.76,1.39,2.02,2.65, 3.7, 4.77, 5.4]
    print("index ",index)
    #index[1,5] - 0.1
    #index = [x-0.1 for (i,x) in enumerate(index) if i<5]
    #opacity = 0.4
    #error_config = {'ecolor': '0.3'}

    barlist = ax.bar(index, data, align='center', width=0.6)
    #barlist = ax.bar(index, data, align='center', alpha=opacity,error_kw=error_config)
    #blue:
    barlist[0].set_color('#31727B')
    barlist[1].set_color('#31727B')
    barlist[2].set_color('#31727B')
    barlist[3].set_color('#31727B')
    barlist[4].set_color('#31727B')
    barlist[5].set_color('#31727B')
    barlist[6].set_color('#31727B')
    barlist[7].set_color('#31727B')
    barlist[8].set_color('#31727B')
    barlist[9].set_color('#31727B')

    barlist[10].set_color('#ff7f0e')
    #Nivida green
    barlist[11].set_color('#84B737')
    barlist[12].set_color('#84B737')


    openmp = mpatches.Patch(color='#31727B', label='OpenMP CPU')
    #openmp = mpatches.Patch(color='#d62728', label='OpenMP')
    offload = mpatches.Patch(color='#ff7f0e', label='OpenMP GPU')
    cuda = mpatches.Patch(color='#84B737', label='CUDA GPU')

    ######### Plot configuration ##########
    ax.set_xticks(index)
    ax.set_xticklabels(labels,fontsize=15,rotation = 45, ha="right", position=(0,-0.03))
    #ax.set_xticklabels(labels,fontsize=10,rotation = 45, va="bottom", position=(0,-0.3))
    ax.set_title("Performance of Optimization Approaches",fontsize=30)
    #ax.set_xlabel("Parallel Approaches")
    ax.set_ylabel("Speed Up Factor",fontsize=20)
    #ax.set_title('Performance of Cell-centered Gradient Kernel on ' + VARIANT_TITLE + ' with different vector instructions')
    #legend(handles=[openmp,cuda,offload], loc='upper right',fontsize=12)
    legend(handles=[openmp,offload,cuda], loc='upper right',fontsize=12)
    fig.tight_layout()
    savefig(PNG_PATH+'/'+PNG_NAME,dpi=400)
    show()



exitapp = False
if __name__ == '__main__':
    try:  
        #Passing the knl data file name here, 19 is the  # of different threads number for knl up to 272
        data =[]
        #labels=['Broadwell ifort','Haswell 4S ifort','Haswell 2S ifort','KNL ifort', 'Power9 gfortran','Power9 xlf','volta Power9 xlf','volta Power9 nvcc','volta Broadwell nvcc']
        labels=['Broadwell gfort','Broadwell ifort','Haswell 4S gfort','Haswell 4S ifort','Haswell 2S gfort','Haswell 2S ifort','KNL gfort','KNL ifort', 'Power9 gfortran','Power9 xlf','volta Power9 xlf','volta Power9 nvcc','volta Broadwell nvcc']
        #Read CPU data
        cori_has =read_file_gpu("Cori_master_haswell_puck_varying_threads_2018-07-20_00-03-14.data",1)
        #aa = float("{0:.2f}".format(round(cori_has/read_file_cpu("bar72_broadwell.data",1),2)))
        print("cori_master_has ",cori_has)
        '''
        #spped up using master_haswell as baseline
        data.append(float("{0:.2f}".format(round(cori_has/read_file_cpu("bar72_broadwell.data",1),2))))
        data.append(float("{0:.2f}".format(round(cori_has/read_file_cpu("bar144_haswell_intel_cores_openmp.data",1),2))))#4 cores
        data.append(float("{0:.2f}".format(round(cori_has/read_file_cpu("cori_bar64_haswell_intel_cores_openmp.data",1),2))))#2 cores
        data.append(float("{0:.2f}".format(round(cori_has/read_file_cpu("cori_bar272_knl_intel_cores_openmp.data",1),2))))        #knl on cori
        data.append(float("{0:.2f}".format(round(cori_has/read_file_cpu("bar160_power9_gcc.data",1),2))))
        data.append(float("{0:.2f}".format(round(cori_has/read_file_cpu("bar160_power9ibm.data",1),2))))
        #Read GPU data
        data.append(float("{0:.2f}".format(round(cori_has/read_file_gpu("offload_ibm_puck_varying_threads_2018-07-18_10-32-44.data",1),2))))
        data.append(float("{0:.2f}".format(round(cori_has/read_file_gpu("power9_cuda_puck_varying_threads_2018-07-10_17-03-06.data",1),2))))
        data.append(float("{0:.2f}".format(round(cori_has/read_file_gpu("cuda_votas_puck_varying_threads_2018-07-18_13-08-01.data",1),2))))
        '''
        '''
        #raw data seconds
        data.append(read_file_cpu("bar72_broadwell.data",1))
        data.append(read_file_cpu("bar144_haswell_intel_cores_openmp.data",1))#4 cores
        data.append(read_file_cpu("cori_bar64_haswell_intel_cores_openmp.data",1))#2 cores
        data.append(read_file_cpu("cori_bar272_knl_intel_cores_openmp.data",1))        #knl on cori
        data.append(read_file_cpu("bar160_power9_gcc.data",1))
        data.append(read_file_cpu("bar160_power9ibm.data",1))
        #Read GPU data
        data.append(read_file_gpu("offload_ibm_puck_varying_threads_2018-07-18_10-32-44.data",1))
        data.append(read_file_gpu("power9_cuda_puck_varying_threads_2018-07-10_17-03-06.data",1))
        data.append(read_file_gpu("cuda_votas_puck_varying_threads_2018-07-18_13-08-01.data",1))
        print("data ",data)
        '''
        plot_bar(labels, data)
        for i in range(len(labels)):
            print(labels[i],": ",data[i])
        print("done")
    except KeyboardInterrupt:
        exitapp = True
        raise             
