#!/bin/bash

# This script countes the number of lines added, changed, and modified between two git commits

HELP_TEXT="\
Usage:
  git_diff_line_count.sh -m <base_commit> -c <commit_list> -f <file_list>
where <commit_list> is a comma-separated list of commit hashes (or commit names)
to compare against <base_commit>, for each file in <file_list> (comma-separated list)."

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
AWK_SCRIPT="$DIR/git_diff_parse.awk"

################################
# Parse command line arguments #
################################
# Set option defaults
COMMIT_OPT=
FILE_OPT=
MASTER=

OPTIND=1  # Reset in case getopts has been used previously in the shell.
while getopts c:f:m: option; do
  case "${option}" in
    c) COMMIT_OPT=${OPTARG}
       ;;
    f) FILE_OPT=${OPTARG}
       ;;
    m) MASTER=${OPTARG}
       ;;
  esac
done

if [ -z "$COMMIT_OPT" ] || [ -z "$FILE_OPT" ] || [ -z "$MASTER" ]; then
    echo "$HELP_TEXT"
    exit 1
fi

# Parse lists
IFS=','
COMMIT_LIST=($COMMIT_OPT)
FILE_LIST=($FILE_OPT)
unset IFS

######################################
# Count line changes for each commit #
######################################
for commit in "${COMMIT_LIST[@]}"; do
  echo "COMPARING '$MASTER' TO '$commit'"
  git diff --word-diff --unified=0 "$MASTER" "$commit" -- ${FILE_LIST[@]} | awk -f $AWK_SCRIPT
done
