# Source for git diff regex: https://stackoverflow.com/questions/9933325/is-there-a-way-of-having-git-show-lines-added-lines-changed-and-lines-removed/9933440
BEGIN {
  first=0;
  a=0; r=0; m=0;
} 

# Modified line
# If line was modified, do not process it again
/^.+(-\]|\+}).*(\[-|{\+).*$/ {m++; next}

# Added line
/^.*{\+.*\+}.*$/ {a++; next}
# Count blank lines
/^\s*$/ {a++; next}

# Removed line
/^.*\[-.*-\].*$/ {r++; next}


# Found file
match($0, /^([\+-]){3} b(.+)/, g) {
  if (first == 1) {
    print a " added"
    print m " modified"
    print r " removed"
  }
  print g[1] g[2]
  first=1;
  a=0; r=0; m=0;
}
#/^([\+-]){3} b/ {print}

END {
  print a " added"
  print m " modified"
  print r " removed"
}
