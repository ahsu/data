#!/bin/sh

#################
# Configuration #
#################
KERNEL_DIR=$HOME/truchas/truchas-kernels
OUTPUT_DIR=$HOME/truchas_results/line_counts

MASTER="origin/master"
BRANCHES=(openmp_orig cuda_orig openmp_orig_offload_one_memcpy)

# Comma-separated list of file paths relative to $KERNEL_DIR
COMMON_FILES=(
src/int-norm/new_interface_normal_function.F90
src/mfd-diff/CMakeLists.txt
src/mfd-diff/new_mfd_disc_type.F90
src/mfd-diff/new_mfd_disc_type.cu
)

#declare -A FILES
#FILES['openmp_orig']

##########
# Script #
##########
# Check directories exist
if [ ! -d "$KERNEL_DIR" ]; then
    echo "Error: kernel directory '$KERNEL_DIR' does not exist"
    exit 1
fi

DATE=$(date '+%Y-%m-%d_%H-%M')
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

GIT_SCRIPT_ALL="$DIR/git_diff_line_count_all.sh"

OUTPUT_DIR=${OUTPUT_DIR}/$DATE
mkdir -p "$OUTPUT_DIR"

################
# Collect data #
################
for branch in "${BRANCHES[@]}"; do
  OUT_NAME="$OUTPUT_DIR/${branch}.data"
  $GIT_SCRIPT_ALL -m $MASTER -c $branch > "$OUT_NAME"  
done
