set(CMAKE_BUILD_TYPE Release CACHE STRING "Build type")
set(CMAKE_C_COMPILER icc CACHE STRING "C compiler")
set(CMAKE_Fortran_COMPILER ifort CACHE STRING "Fortran compiler")
set(CMAKE_Fortran_FLAGS "-u $ENV{VECTOR_INST} -qopt-report" CACHE STRING "Fortran compile flags")
set(CMAKE_Fortran_FLAGS_RELEASE "-O3"
    CACHE STRING "Fortran flags for Release builds")
