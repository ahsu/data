#!/usr/bin/env python
from os.path import expanduser
from os import listdir
import re

import numpy as np
from matplotlib.pyplot import figure, savefig, show

HOME = expanduser("~")
PDF_PATH = HOME+"/truchas/data/data/vector_instructions/figures"


# Name of kernel executable file
# KERNEL_EXE='int-norm-driver'
# DATA_PATH = HOME+"/truchas/data/data/vector_instructions/2018-07-14_haswell_32_knl_272/int-norm-driver"
# PDF_NAME = "vector_instructions_int_norm_haswell_32_knl_272.pdf"

# Name of kernel executable file
KERNEL_EXE='mfd-diff-test'
DATA_PATH = HOME+"/truchas/data/data/vector_instructions/2018-07-30_haswell_32_knl_272/mfd_apply_diff"
PDF_NAME = "vector_instructions_mfd_diff_haswell_32_knl_272.pdf"

PLOT_TITLE = 'Performance of Different Vector Instructions'
FONT_SIZES = {'title': 30, 'axis_labels': 20, 'tick_labels': 12, 'legend': 12}

LABEL_ORDER_KEYS = ('-no-vec', '-xSSE4.2', '-xCORE-AVX2', '-xMIC-AVX512')
LABEL_ORDER_VALS = ('-no-vec\n0 bytes', '-xSSE4.2\n128 bytes', '-xCORE-AVX2\n256 bytes', '-xMIC-AVX512\n512 bytes')

KNL_CONFIG = {'label' : 'KNL', 'color' : 'r', 'path' : DATA_PATH + '/knl'}
HASWELL_CONFIG = {'label' : 'Haswell', 'color' : 'b', 'path' : DATA_PATH + '/haswell'}

###################################################################################
#  Function definitions
###################################################################################
def read_file(input_file):
    lines = open(input_file).read().split('\n')
    sec_per_iter = []
    for i in range(0, len(lines) - 1, 3):
        num_iters = int(lines[i])
        time_str = re.search(r'^new kernel: (.*)', lines[i+2]).group(1)
        time = float(time_str)
        sec_per_iter.append(time / num_iters)
    return sec_per_iter

def get_variant_data(input_path):
    labels = []
    data = []
    std_dev = []
    for filename in listdir(input_path):
        search_obj = re.search(r'^' + KERNEL_EXE + r'-([\w\-\.]+)_\d{4}',filename)
        if search_obj:
            labels.append(search_obj.group(1))
            file_data = read_file(input_path + '/' + filename)
            avg_sec_per_iter = np.mean(file_data)
            error = np.std(file_data)
            data.append(avg_sec_per_iter)
            std_dev.append(error)
        else:
            print("Error: File name '" + filename + "' does not have the correct format")
            continue
    return {'labels' : labels, 'data' : data, 'std_dev' : std_dev}

def reorder_data(variant):
    # Add empty data for missing labels
    for name in LABEL_ORDER_KEYS:
      if name not in variant['labels']:
        variant['labels'].append(name)
        variant['data'].append(0.0)
        variant['std_dev'].append(0.0)

    # Reorder data
    reorder_idx = [variant['labels'].index(name) for name in LABEL_ORDER_KEYS]
    variant['labels'] = [variant['labels'][i] for i in reorder_idx]
    variant['data'] = [variant['data'][i] for i in reorder_idx]
    variant['std_dev'] = [variant['std_dev'][i] for i in reorder_idx]

def generate_plot(haswell_data, knl_data):
    fig = figure(figsize=(10, 5))
    ax = fig.add_subplot(111)

    # Evenly space plots
    index = np.arange(len(LABEL_ORDER_VALS))

    bar_width = 0.35
    opacity = 0.6
    error_config = {'ecolor': '0.3'}

    has_bar = ax.bar(index, haswell_data['data'], bar_width, alpha=opacity, yerr=haswell_data['std_dev'], error_kw=error_config, label=HASWELL_CONFIG['label'], color=HASWELL_CONFIG['color'])
    knl_bar = ax.bar(index+bar_width, knl_data['data'], bar_width, alpha=opacity, yerr=knl_data['std_dev'], error_kw=error_config, label=KNL_CONFIG['label'], color=KNL_CONFIG['color'])

    ######### Plot configuration ##########
    ax.set_xticks(index + bar_width)
    ax.set_xticklabels(LABEL_ORDER_VALS, fontsize=FONT_SIZES['tick_labels'])
    ax.legend(loc='best', fontsize=FONT_SIZES['legend'])
    ax.set_xlabel("Intel compiler (ifort) flag", fontsize=FONT_SIZES['axis_labels'])
    ax.set_ylabel("Run Time (Seconds)", fontsize=FONT_SIZES['axis_labels'])
    #ax.set_title(PLOT_TITLE, fontsize=FONT_SIZES['title'])

    fig.tight_layout()
    savefig(PDF_PATH + '/' + PDF_NAME, dpi=400)
    show()

###################################################################################
#  Main
###################################################################################
exitapp = False
if __name__ == '__main__':
    try:
        haswell_data = get_variant_data(HASWELL_CONFIG['path'])
        knl_data = get_variant_data(KNL_CONFIG['path'])

        print("DATA READ:")
        print('HASWELL:', haswell_data)
        print('KNL:    ', knl_data)

        print("\n\nREORDERED DATA:")
        reorder_data(haswell_data)
        reorder_data(knl_data)
        print('HASWELL:', haswell_data)
        print('KNL:    ', knl_data)

        # Plot data
        generate_plot(haswell_data, knl_data)

    except KeyboardInterrupt:
        exitapp = True
        raise
