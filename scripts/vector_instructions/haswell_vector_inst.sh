#!/bin/bash -e                                                                                                                                                                                                                                                                
# NOTE: This script is designed to run on Cori, some changes may be necessary for other systems

#SBATCH --nodes=1                          # number of nodes
#SBATCH --constraint=haswell               # node constraints
#SBATCH --qos=regular                      # quality of service
#SBATCH --time=06:00:00                    # walltime
#SBATCH --job-name=haswell_vec             # job name
#SBATCH --output=haswell_vector_inst.out   # output file name
#SBATCH --mail-user=dneillasanza@lanl.gov  # email address
#SBATCH --mail-type=END                    # send end of job email

##########
# Config #
##########

# Set vector instruction variant
VARIANT="haswell"
VECTOR_INSTS=(-xSSE4.2 -xCORE-AVX2 -no-vec)
NUM_THREADS=32
runs=10

OUTPUT_DIR="$HOME/truchas_results/vector_instructions/$VARIANT"

KERNEL_DIR="$HOME/truchas/truchas-kernels"
TPL_DIR=$HOME/truchas/truchas-kernels-tpl/build_${VARIANT}_intel/install

# Path to kernel executable relative to cmake build directory
#RELATIVE_EXE_PATH=/src/int-norm/int-norm-driver-data
RELATIVE_EXE_PATH="src/mfd-diff/mfd-diff-test"

###########
#   OMP   #
###########
export OMP_DISPLAY_ENV=false
export OMP_PROC_BIND=spread
export OMP_PLACES=cores 
export OMP_NUM_THREADS=$NUM_THREADS 

###########
# Modules #
###########

##########
# Script #
##########
# Check directories exist
if [ ! -d "$TPL_DIR" ]; then
      echo "Error: TPL directory '$TPL_DIR' does not exist"
      exit 1
fi

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SCRIPT_DIR=$HOME/truchas/data/scripts/vector_instructions
CMAKE_CONFIG_PATH=$SCRIPT_DIR/intel-vector-inst.cmake

EXE_NAME=$( basename  "$RELATIVE_EXE_PATH")

BIN_DIR="$SCRIPT_DIR/bin/$VARIANT"
mkdir -p "$BIN_DIR"

mkdir -p "$OUTPUT_DIR"

###########
# Compile #
###########
for vi in "${VECTOR_INSTS[@]}"; do
  BUILD_DIR=$KERNEL_DIR/build_data_$VARIANT\_$vi
  
  rm -rf "$BUILD_DIR"
  mkdir "$BUILD_DIR"
  cd "$BUILD_DIR"
  
  # Set vector instruction type
  export VECTOR_INST="$vi"
  
  # Compile code  
  cmake -C $CMAKE_CONFIG_PATH -D CMAKE_PREFIX_PATH=$TPL_DIR ..
  make -j 8 VERBOSE=1
 
  cp "$BUILD_DIR/$RELATIVE_EXE_PATH" "$BIN_DIR/$EXE_NAME-$vi"
done

#######
# Run #
#######
# For running on Cori
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$TPL_DIR/lib

DATE=$(date +'%Y-%m-%d_%H-%M-%S')
ARGS="$KERNEL_DIR/meshes/puck-cast.exo new"

for EXE in $BIN_DIR/*; do
  OUTPUT_NAME="$(basename $EXE)_$DATE.data"
  for i in $(seq 1 $runs); do
    $EXE $ARGS
    if [ $? -ne 0 ]; then
      echo "CRITICAL: Failed at run"
      exit 1
    fi
  done > "$OUTPUT_DIR/$OUTPUT_NAME"
done 
