#!/usr/bin/env python
import numpy as np
from matplotlib.pyplot import figure, savefig, plot, show, legend, xlabel, ylabel
in_lable_knl="knl_puck"
in_lable_has="haswell_puck"
#in_lable_knl="knl_small"
#in_lable_has="haswell_small"
png_path="/global/homes/a/abhsu/results_truchas/data/data/varying_threads/png/"
input_path="/global/homes/a/abhsu/results_truchas/data/data/varying_threads/"
png_name="C_both_puck_has64_loglog.png"
fig = figure(figsize=(10,5))
ax = fig.add_subplot(111)

def read_file(input_file,kernel_iters):
	hannel_values = open(input_path+input_file).read().split()
	iters =kernel_iters
	size=len(hannel_values)//iters//5
	print("size ", size, "len(hannel_values) ", len(hannel_values))
	nub_threads_list =[]
	avg_iter_list=[]
	avg_time_list=[]
	time_iter_list=[]
	for j in range(iters):
		num_iter=[]
		time=[]
		for i in range(size):
			#print("i ",i)
			num_iter.append(int(hannel_values[1+i*5+j*50]))
			time.append(float(hannel_values[4+i*5+j*50]))
		avg_iter = np.mean(num_iter)
		avg_time = np.mean(time)
		nub_threads_list.append(hannel_values[j*50])
		avg_iter_list.append(avg_iter)
		avg_time_list.append(avg_time)
		time_iter_list.append(avg_time/avg_iter)
		print(hannel_values[j*50],avg_iter, avg_time, avg_time/avg_iter)
		#print(hannel_values[j*50],avg_iter, avg_time, avg_time/avg_iter, file=out_file)
	return nub_threads_list,avg_iter_list,avg_time_list,time_iter_list





###################################################################################
#PLOT
def plot_threads(knl_nub_threads_list,knl_time_iter_list,has_nub_threads_list,has_time_iter_list):
	ax.plot(has_nub_threads_list, has_time_iter_list/has_time_iter_list[0], label=in_lable_has)
	ax.plot(knl_nub_threads_list, knl_time_iter_list/knl_time_iter_list[0], label=in_lable_knl)
	x_min=int(knl_nub_threads_list[0])
	x_max=int(knl_nub_threads_list[-1])
	x=[x_min,x_max]
	y=[x_min**-1,x_max**-1]
	ax.plot(x, y, label='ideal scaling line',linestyle='--')
	######### Plotting adjustments ##########
	ax.set_xscale('log')
	ax.set_yscale('log')
	ax.set_xlabel("# of threads",fontsize=30)
	ax.set_ylabel("Relative Time",fontsize=30,labelpad=15)
	ax.legend(frameon=False,handlelength=4,fontsize=20,loc=3)
	ax.xaxis.set_tick_params(labelsize=20)
	ax.yaxis.set_tick_params(labelsize=20)
	fig.tight_layout()
	savefig(png_path+png_name,dpi=400)

exitapp = False
if __name__ == '__main__':
	try:  
		#Passing the knl data file name here, 19 is the  # of different threads number for knl up to 272
		knl_nub_threads_list,knl_avg_iter_list,knl_avg_time_list,knl_time_iter_list= read_file("C_knl_puck_varying_threads_2018-07-01_23-14-18.data",19)
		assert(len(knl_avg_iter_list)==len(knl_avg_time_list)==len(knl_time_iter_list) == len(knl_nub_threads_list))
		print("haswell ")
		#Passing the haswell data file name here, 9 is the  # of different threads number for haswell up to 64
		has_nub_threads_list,has_avg_iter_list,has_avg_time_list,has_time_iter_list= read_file("C_has_puck_varying_threads_2018-07-02_00-23-34.data",9)
		assert(len(has_avg_iter_list)==len(has_avg_time_list)==len(has_time_iter_list) == len(has_nub_threads_list))
		plot_threads(knl_nub_threads_list,knl_time_iter_list,has_nub_threads_list,has_time_iter_list)
		print("done")
	except KeyboardInterrupt:
		exitapp = True
		raise             
