#!/bin/bash -e                                                                                                                                                                                                                                                                
# NOTE: This script is designed to run on Cori, some changes may be necessary for other systems

#SBATCH --nodes=1                           # number of nodes
#SBATCH --constraint=knl,quad,cache                # node constraints
#SBATCH --qos=regular                       # quality of service
#SBATCH --time=08:00:00                      # walltime
#SBATCH --job-name=knl_puck           # job name
#SBATCH --output=knl_profiling.txt      # output file name
#SBATCH --mail-user=abigail.hsu@stonybrook.edu  # email address
#SBATCH --mail-type=END                     # send end of job email

##########
#Load #
##########
source $HOME/spack_setup.sh



##########
# Config #
##########

VARIANT=knl
num_threads=(1 2 4 8 16 17 24 34 51 64 68 85 102 119 136 170 204 238 272)
runs=10

OUTPUT_DIR=$HOME/results_truchas/data/data/varying_threads

KERNEL_DIR=$HOME/truchas/truchas-kernels
TPL_DIR=$HOME/truchas/truchas-kernels-tpl/build-cori/install

###########
#   OMP   #
###########
export OMP_DISPLAY_ENV=false
export OMP_PROC_BIND=spread
export OMP_PLACES=cores 


##########
# Script #
##########
BUILD_DIR="$KERNEL_DIR/build_cori_$VARIANT"
DATE=$(date +'%Y-%m-%d_%H-%M-%S')
#FIXME, update name here
#OUTPUT_NAME="C_knl_small_varying_threads_$VARIANT_$DATE.data"
OUTPUT_NAME="C_knl_puck_varying_threads_$VARIANT_$DATE.data"
#OUTPUT_NAME="orig_knl_puck_varying_threads_$VARIANT_$DATE.data"
#OUTPUT_NAME="orig_knl_small_varying_threads_$VARIANT_$DATE.data"

cd $KERNEL_DIR

rm -rf $BUILD_DIR
mkdir $BUILD_DIR
cd $BUILD_DIR

cmake -C ../config/intel-opt.cmake -D CMAKE_PREFIX_PATH=$TPL_DIR ..                                                                                                                                                                                                         
make

# For running on Cori
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$TPL_DIR/lib

EXE="$BUILD_DIR/src/int-norm/int-norm-driver"
#ARGS="$KERNEL_DIR/meshes/mesh1-fine.exo new"
ARGS="$KERNEL_DIR/meshes/puck-cast.exo new"

for nt in "${num_threads[@]}"; do
  export OMP_NUM_THREADS=$nt
  
  for i in $(seq 1 $runs); do
    echo -e "$nt"
    #echo -e "$EXE $ARGS"
    $EXE $ARGS
    if [ $? -ne 0 ]; then
    echo "CRITICAL: Failed at run"
    exit 1
    fi
  done

done > "$OUTPUT_DIR/$OUTPUT_NAME"
