# Source: based on Intel's 'roofline.py' example script included with Intel Advisor

import sys
import math
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

import re
from pprint import pprint

try:
    import advisor
except ImportError:
    print('Import error: Python could not load advisor python library. Possible reasons:\n'
          '1. Python cannot resolve path to Advisor\'s pythonapi directory. '
          'To fix, either manually add path to the pythonapi directory into PYTHONPATH environment variable,'
          ' or use advixe-vars.* scripts to set up product environment variables automatically.\n'
          '2. Incompatible runtime versions used by advisor python library and other packages '
          '(such as matplotlib or pandas). To fix, either try to change import order or update other package '
          'version if possible.')
    sys.exit(1)

pd.options.display.max_rows = 20

################
# Plot options #
################
FONT_SIZES = {'title': 30, 'axis_labels': 15, 'legend': 8, 'annotation': 8}
LABEL_REORDER = (4, 6, 5, 0, 1, 2, 3, 7)

# Roofline for loop
# X_AXIS = 'self_ai'
# Y_AXIS = 'self_gflops'

# Roofline with callstacks
X_AXIS = 'total_arithmetic_intensity'
Y_AXIS = 'total_gflops'


################
#    Script    #
################
if len(sys.argv) < 2:
    print('Usage: "python {} path_to_project_dir"'.format(__file__))
    sys.exit(2)

project = advisor.open_project(sys.argv[1])
data = project.load(advisor.SURVEY)
rows = [{col: row[col] for col in row} for row in data.bottomup]
roofs = data.get_roofs()

df = pd.DataFrame(rows).replace('', np.nan)
print(df[['loop_name', X_AXIS, Y_AXIS]].dropna())

df[X_AXIS] = df[X_AXIS].astype(float)
df[Y_AXIS] = df[Y_AXIS].astype(float)

width = df[X_AXIS].max() * 1.2

fig, ax = plt.subplots()
max_compute_roof = max(roofs, key=lambda roof: roof.bandwidth if 'bandwidth' not in roof.name.lower() else 0)
max_compute_bandwidth = max_compute_roof.bandwidth / math.pow(10, 9)  # converting to GByte/s

for roof in roofs:
    # drawing multi threaded roofs only
    if 'single-thread' not in roof.name:
        # memory roofs
        if 'bandwidth' in roof.name.lower():
            bandwidth = roof.bandwidth / math.pow(10, 9) # converting to GByte/s
            # y = banwidth * x
            x1, x2 = 0, min(width, max_compute_bandwidth / bandwidth)
            y1, y2 = 0, x2 * bandwidth
            label = '{} {:.0f} GB/s'.format(roof.name, bandwidth)
            ax.plot([x1, x2], [y1, y2], '-', label=label)

        # compute roofs, ignoring single-precision roofs
        else:
            if 'sp' not in roof.name.lower():
                bandwidth = roof.bandwidth / math.pow(10, 9)  # converting to GFlOPS
                x1, x2 = 0, width
                y1, y2 = bandwidth, bandwidth
                label = '{} {:.0f} GFLOPS'.format(roof.name, bandwidth)
                ax.plot([x1, x2], [y1, y2], '-', label=label)


# drawing points using the same ax
ax.set_xscale('log', nonposx='clip')
ax.set_yscale('log', nonposy='clip')
ax.plot(df[X_AXIS], df[Y_AXIS], 'o', label='Loop or Subroutine', color='c')

# Plot points of interest
matches = filter(lambda row: re.match(r'^new_interface', row['mangled_name']), rows)

for match in matches:
    x_val = float(match[X_AXIS])
    y_val = float(match[Y_AXIS])

    ax.plot(x_val, y_val, 'o', color='xkcd:gold')
    plt.annotate(
        'Gradient Kernel',
        fontsize=FONT_SIZES['annotation'],
        xy=(x_val, y_val), xytext=(40, 20),
        textcoords='offset points', ha='right', va='bottom',
        bbox=dict(boxstyle='round,pad=0.5', fc='xkcd:gold', alpha=0.5),
        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))


######################
# Plot configuration #
######################
ax.set_xlabel("Arithmetic Intensity (FLOP/Byte)", fontsize=FONT_SIZES['axis_labels'])
ax.set_ylabel("Performance (GFLOP/s)", fontsize=FONT_SIZES['axis_labels'])
ax.grid(linestyle='dashed', linewidth=1)

# Manually reorder plot legend
handles, labels = ax.get_legend_handles_labels()
handles = [handles[idx] for idx in LABEL_REORDER]
labels = [labels[idx] for idx in LABEL_REORDER]

# Put a legend to the right of the current axis
plt.legend(handles, labels, loc='lower left', fancybox=True, prop={'size': FONT_SIZES['legend']})
fig.tight_layout()

# saving the chart in PDF format
plt.savefig('roofline.pdf')
plt.show()

print('Roofline chart has been generated and saved into roofline.png and roofline.svg files in the current directory')
