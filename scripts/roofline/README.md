# Methodology for collecting Performance Portability Metrics

## CUDA approaches

Compile the kernel and run
```sh
nvprof -m dram_write_throughput,dram_read_throughput,dram_utilization,global_replay_overhead,global_cache_replay_overhead $EXE $MESH new
```
where `EXE` is the kernel executable and `MESH` is the path to the chosen mesh file.
