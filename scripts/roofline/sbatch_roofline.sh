#!/bin/bash
# NOTE: This script is designed to run on Cori, some changes may be necessary for other systems

#SBATCH --nodes=1                           # number of nodes
#SBATCH --constraint=knl,quad,cache               # node constraints
#SBATCH --qos=regular                       # quality of service
#SBATCH --time=2:00:00                      # walltime
#SBATCH --job-name=roofline                 # job name
#SBATCH --output=truchas_profiling.txt      # output file name
#SBATCH --mail-user=dneillasanza@lanl.gov   # email address
#SBATCH --mail-type=END                     # send end of job email

# Set execution variables and file paths
EXE_PATH=$PROFILE_EXE_PATH
ARGS=$PROFILE_ARGS

TRUCHAS_TPL_INSTALL=$HOME/truchas/truchas-kernels-tpl/build_${VARIANT}/install

# Create output directory
DATE=$(date '+%Y-%m-%d_%H-%M')
OUTPUT_DIR="$HOME/profiling/$PROFILE_EXE_NAME/$DATE"

mkdir -p "$OUTPUT_DIR"
cd "$OUTPUT_DIR"

# Setup OpenMP variables
export OMP_NUM_THREADS=$NTHREADS
export OMP_PLACES=cores
export OMP_PROC_BIND=spread

# Dynamically link to truchas TPLs
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$TRUCHAS_TPL_INSTALL/lib

###########
# Advisor #
###########
module load advisor
advixe-cl -c roofline -project-dir="$OUTPUT_DIR/advisor" -- $EXE_PATH $ARGS
advixe-cl -c survey -project-dir="$OUTPUT_DIR/advisor" -- $EXE_PATH $ARGS
advixe-cl -c tripcounts -flop -stacks -project-dir="$OUTPUT_DIR/advisor" -- $EXE_PATH $ARGS
