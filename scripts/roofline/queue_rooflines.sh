#!/bin/bash

# A script to collect rooflines for each binary in BIN_DIR

###################################
#            IMPORTANT            #
###################################
#  MAKE SURE YOU SET THE CORRECT  #
#    HARDWARE CONSTRAINT IN       #
#     'sbatch_roofline.sh'        #
###################################

# Set file paths
# BIN_DIR=$HOME/truchas/bin/no_vec/haswell
BIN_DIR=$HOME/truchas/bin/skylake
ARGS="$HOME/truchas/truchas-kernels/meshes/puck-cast.exo new"

# Set sbatch script parameters
export NTHREADS=112
export VARIANT=skylake_intel

for exe in "$BIN_DIR"/*; do
  export PROFILE_EXE_NAME=$(basename "$exe")
  export PROFILE_EXE_PATH="$exe"
  export PROFILE_ARGS="$ARGS"
  #sbatch $HOME/truchas/data/scripts/roofline/sbatch_roofline.sh
  $HOME/truchas/data/scripts/roofline/sbatch_roofline.sh
done
