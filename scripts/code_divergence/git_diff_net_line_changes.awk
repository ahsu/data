BEGIN { a=d=0 }; 

# Ignore git notes
match($3, /[0-9a-f]{40}/) {next}

{ a+=$1; d+=$2 }; 

END {
    print "Added: " a; 
    print "Removed: " d; 
    print "Net: " a-d
}
