#!/bin/bash

# This script countes the number of net lines changes (added - removed) between two git commits

HELP_TEXT="\
Usage:
  git_diff_net_line_count.sh -b <base_commit> -t <target_commit>
where <base_commit> and <target_commit are two commits"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
AWK_SCRIPT="$DIR/git_diff_net_line_changes.awk"

################################
# Parse command line arguments #
################################
# Set option defaults
BASE=
TARGET=

OPTIND=1  # Reset in case getopts has been used previously in the shell.
while getopts b:t: option; do
  case "${option}" in
    b) BASE=${OPTARG}
       ;;
    t) TARGET=${OPTARG}
       ;;
  esac
done

if [ -z "$BASE" ] || [ -z "$TARGET" ]; then
    echo "$HELP_TEXT"
    exit 1
fi

######################################
# Count line changes between commits #
######################################
echo "COMPARING '$BASE' TO '$TARGET'"
git diff --ignore-space-at-eol -b -w --ignore-blank-lines --numstat "$BASE" "$TARGET" | awk -f $AWK_SCRIPT

echo

echo "COMPARING '$TARGET' TO '$BASE'"
git diff --ignore-space-at-eol -b -w --ignore-blank-lines --numstat "$TARGET" "$BASE" | awk -f $AWK_SCRIPT
